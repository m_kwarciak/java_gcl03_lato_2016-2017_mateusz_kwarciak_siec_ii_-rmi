package Server;

import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Mateusz on 14.05.2017.
 */
public interface CrawlerMethods extends Remote{

    public void start() throws InterruptedException, IOException, CrawlerException;
    public void setMessageEvent( MessageEvent event ) throws RemoteException;
    public void sendMessages(String txt) throws RemoteException;
}

package Server;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Mateusz on 20.03.2017.
 */
public interface Logger extends Remote{
    void log(String status, Student student) throws RemoteException;
}
